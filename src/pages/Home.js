import React from 'react';
import Navbar from '../components/Navbar';

function Home() {
	return (
		<div>
			<Navbar />
			<div className="flex justify-center h-96 items-center text-6xl font-bold"> ini adalah home screen</div>
		</div>
	);
}

export default Home;
