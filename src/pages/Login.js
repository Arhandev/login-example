import axios from 'axios';
import { Formik } from 'formik';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import Navbar from '../components/Navbar';

const validationSchema = Yup.object({
	email: Yup.string().required().email(),
	password: Yup.string().required(),
});

function Login() {
	const initialState = {
		email: '',
		password: '',
	};
	const navigate = useNavigate();
	const onSubmit = async values => {
		try {
			const response = await axios.post('http://localhost:8000/api/login', {
				email: values.email,
				password: values.password,
			});
			localStorage.setItem('token', response.data.data.token);
			localStorage.setItem('username', response.data.data.user.username);
			navigate('/');
		} catch (error) {
			alert(error.response.data.info);
		}
	};
	return (
		<div>
			<Navbar />
			<div className="border-2 border-black rounded-2xl p-6 max-w-xl mx-auto mt-16">
				<h1 className="text-center font-bold text-2xl">Login</h1>
				<Formik onSubmit={onSubmit} initialValues={initialState} validationSchema={validationSchema}>
					{({ handleSubmit, handleBlur, handleChange, values, errors, touched }) => (
						<form onSubmit={handleSubmit} className="mx-8 flex flex-col gap-4 my-8 items-center">
							<div className="w-full grid grid-cols-3 text-lg">
								<label className="">Email:</label>
								<input
									className="col-span-2 border-black border-2 rounded-md"
									name="email"
									type="text"
									onChange={handleChange}
									value={values.email}
									onBlur={handleBlur}
								/>
								<div></div>
								<div className="col-span-2">{touched.email && errors.email}</div>
							</div>
							<div className="w-full grid grid-cols-3 text-lg">
								<label className="">Password:</label>
								<input
									className="col-span-2 border-black border-2 rounded-md"
									name="password"
									type="password"
									onChange={handleChange}
									value={values.password}
									onBlur={handleBlur}
								/>
								<div></div>
								<div className="col-span-2">{touched.password && errors.password}</div>
							</div>
							<button className="px-4 py-2 bg-blue-400 text-white rounded-xl font-bold text-lg mt-4">Login</button>
						</form>
					)}
				</Formik>
			</div>
		</div>
	);
}

export default Login;
