import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Navbar from '../components/Navbar';

function Profile() {
	const [user, setUser] = useState({});
	const fetchProfile = async () => {
		try {
			const token = localStorage.getItem('token');
			const response = await axios.get('http://localhost:8000/api/profile', {
				headers: { Authorization: `Bearer ${token}` },
			});
			setUser(response.data.data.user);
		} catch (err) {
			console.log(err);
			alert(err.response.data.message);
		}
	};
	useEffect(() => {
		fetchProfile();
	}, []);

	return (
		<div>
			<Navbar />
			<div className="max-w-2xl mx-auto p-8 border-2 border-black rounded-xl my-12">
				<h1 className="text-3xl font-bold">Profile</h1>
				<div className="grid grid-cols-3 gap-y-4 mt-8 text-lg">
					<p>Nama:</p>
					<p className="col-span-2">{user.name}</p>
					<p>email:</p>
					<p className="col-span-2">{user.email}</p>
					<p>username:</p>
					<p className="col-span-2">{user.username}</p>
				</div>
			</div>
		</div>
	);
}

export default Profile;
