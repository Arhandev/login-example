import axios from 'axios';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

function Navbar() {
	const navigate = useNavigate();
	const onLogout = async () => {
		try {
			const token = localStorage.getItem('token');
			const response = await axios.post(
				'http://localhost:8000/api/logout',
				{},
				{ headers: { Authorization: `Bearer ${token}` } }
			);
			localStorage.removeItem('token');
			localStorage.removeItem('username');
			navigate('/');
		} catch (error) {
			alert(error.response.data.info);
		}
	};
	return (
		<div className="bg-blue-900">
			<header className="w-10/12 mx-auto py-6 flex justify-between items-center">
				<nav className="flex items-center gap-8 text-white text-2xl">
					<div>
						<Link to={'/'}>Home</Link>
					</div>
					<div>
						<Link to={'/profile'}>Profile</Link>
					</div>
				</nav>
				<nav>
					{localStorage.getItem('token') === null ? (
						<Link to={'/login'}>
							<button className="bg-blue-200 border-2 border-blue-200 px-7 py-2 rounded-xl text-xl text-blue-900 font-bold">
								Login
							</button>
						</Link>
					) : (
						<div className="flex items-center gap-4">
							<div className="text-white text-2xl">Halo, {localStorage.getItem('username')}</div>
							<button
								onClick={onLogout}
								className="bg-white border-2 border-red-600 px-7 py-2 rounded-xl text-xl text-red-600 font-bold"
							>
								Logout
							</button>
						</div>
					)}
				</nav>
			</header>
		</div>
	);
}

export default Navbar;
